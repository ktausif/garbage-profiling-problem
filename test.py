import numpy as np
import pip
import tensorflow as tf
import sys
import json
option=""
IMGPATH=""
typeofimage=""
model_file="model/retrained_graph.pb"
input_height = 299
input_width = 299
input_mean = 0
input_std = 255
input_layer = "input"
output_layer = "InceptionV3/Predictions/Reshape_1"
file_name = ""
label_file = "model/retrained_labels.txt"
input_layer ="Placeholder"
output_layer ="final_result"

def load_graph(model_file):
    graph = tf.Graph()
    graph_def = tf.GraphDef()
    try:
        with open(model_file, "rb") as f:
            graph_def.ParseFromString(f.read())
    except:
        with pickle.load(open(model_file, "rb" )) as f:
            graph_def.ParseFromString(f.read())
    with graph.as_default():
        tf.import_graph_def(graph_def)
    return graph


def read_tensor_from_image_file(file_name,input_height=299,input_width=299,input_mean=0,input_std=255):
    input_name = "file_reader"
    output_name = "normalized"
    file_reader = tf.read_file(file_name, input_name)
    if file_name.endswith(".png"):
        image_reader = tf.image.decode_png(file_reader, channels=3, name="png_reader")
    elif file_name.endswith(".gif"):
        image_reader = tf.squeeze(tf.image.decode_gif(file_reader, name="gif_reader"))
    elif file_name.endswith(".bmp"):
        image_reader = tf.image.decode_bmp(file_reader, name="bmp_reader")
    else:
        image_reader = tf.image.decode_jpeg(file_reader, channels=3, name="jpeg_reader")
    float_caster = tf.cast(image_reader, tf.float32)
    dims_expander = tf.expand_dims(float_caster, 0)
    resized = tf.image.resize_bilinear(dims_expander, [input_height, input_width])
    normalized = tf.divide(tf.subtract(resized, [input_mean]), [input_std])
    sess = tf.compat.v1.Session()
    result = sess.run(normalized)
    return result

def read_tensor_from_image_url(url,
                               input_height=299,
                               input_width=299,
                               input_mean=0,
                               input_std=255):
    input_name = "file_reader"
    output_name = "normalized"
    image_reader = tf.image.decode_jpeg(
        requests.get(url).content, channels=3, name="jpeg_reader")
    float_caster = tf.cast(image_reader, tf.float32)
    dims_expander = tf.expand_dims(float_caster, 0)
    resized = tf.image.resize_bilinear(dims_expander, [input_height, input_width])
    normalized = tf.divide(tf.subtract(resized, [input_mean]), [input_std])
    sess = tf.compat.v1.Session()
    return sess.run(normalized)

def load_labels(label_file):
    label = []
    proto_as_ascii_lines = tf.gfile.GFile(label_file).readlines()
    for l in proto_as_ascii_lines:
        label.append(l.rstrip())
    return label


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS
def predict(img,opt):
    ##algo for model output
    global model_file
    
    file_name=img
    graph = load_graph(model_file)
    if(option!="url"):
        t = read_tensor_from_image_file(
          file_name,
          input_height=input_height,
          input_width=input_width,
          input_mean=input_mean,
          input_std=input_std)
    else:
        t = read_tensor_from_image_url(
          file_name,
          input_height=input_height,
          input_width=input_width,
          input_mean=input_mean,
          input_std=input_std)
    input_name = "import/" + input_layer
    output_name = "import/" + output_layer
    input_operation = graph.get_operation_by_name(input_name)
    output_operation = graph.get_operation_by_name(output_name)

    with tf.compat.v1.Session(graph=graph) as sess:
        results = sess.run(output_operation.outputs[0], {input_operation.outputs[0]: t})
    results = np.squeeze(results)
    top_k = results.argsort()#[-5:][::-1]
    labels = load_labels(label_file)
    label=[]
    outcome={}
    for i in top_k:
        #print(labels[i],"{0:.2f}%".format(results[i]*100))
        outcome[labels[i]]="{0:.2f}".format(results[i]*100)
    return outcome


def print_data():
    global IMGPATH
    global option
    result=predict(IMGPATH,option)
    wet=["fastfood","rotten food","flower","food","mutton","plant","wetwaste","edibles"] 
    dry=["cardboard","news paper","crumpled_paper","crumpled paper","disposable_paper_cups","disposable paper cups","foil","leaves","glass","glass_bottle","metal","metal containers","paper","receipt","trash","newspapers","scrap","ewaste","books","cans"] 
    medical=["diaper","medical_waste","sanitary_napkins","medicine","sanitary napkins","syring"]
    plastic=["egg_packaging","plastic","plastic_bottle","plastic bottle"]
    wet_per=0
    dry_per=0
    medical_per=0
    plastic_per=0
    wet_dict={}
    dry_dict={}
    medical_dict={}
    plastic_dict={}
    for key, value in result.items():
        if key in wet:
            wet_per=wet_per+float(value)
            wet_dict[key]=value
        if key in dry:
            dry_per=dry_per+float(value)
            dry_dict[key]=value
        if key in medical:
            medical_per=medical_per+float(value)
            medical_dict[key]=value
        if key in plastic:
            plastic_per=plastic_per+float(value)
            plastic_dict[key]=value
    dry_per=dry_per+plastic_per
    rating=(abs((wet_per/100)-(dry_per/100))*2.5)+(((100-plastic_per)/100)*2.5)
    dry_dict.update(plastic_dict)
    #print(wet_per,dry_per,rating)
    
    return rating,wet_per,dry_per,plastic_per,wet_dict,dry_dict,plastic_dict


def data():
    global IMGPATH
    global option
    global typeofimage
    typeofimage=sys.argv[1]
    option=sys.argv[2]
    if(option=="file"):
        file=sys.argv[3]
        IMGPATH="uploads/"+file
        result=print_data()
    elif(option=="urls"):
        urls=sys.argv[3]
        IMGPATH=urls
        result=print_data()
    elif(option=="dropdown"):
        data=sys.argv[3]
        IMGPATH="uploads/"+data+".PNG"
        result=print_data()
    else:
        result=["abc",123]
    l=["abc",123,13.5]
    #print(typeofimage,option," by python")
    print (result)

    


data()






